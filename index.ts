import dotenv from 'dotenv'
import server from './src/server'
import { LogError, LogSuccess } from './src/utils/logger'

// * configuration the .env
dotenv.config()

const port = process.env.PORT || 8000

// * Execute server

server.listen(port, () => {
  LogSuccess(`[SERVER ON]: Running on http://localhost:${port}/api`)
})

// * Control Server Error
server.on('error', (error) => {
  LogError(`[SERVER ERROR]: ${error}`)
})
