# code-backend-verifier-mern-bootcamp

Node Express project - backend

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/gabirobert32/code-backend-verifier-mern-bootcamp.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/gabirobert32/code-backend-verifier-mern-bootcamp/-/settings/integrations)

## Actividades Open Bootcamp

- Nodemon: se instala esta dependencia para estar refrescando el server en cuanto se aplican cambios al proyecto o que no se pare el mismo cuando hay un error.
- Typescript: Se instala y el TSC se utiliza para transpilar el código desde TS a JS (más sus dependencias para Express y Node). Lo que nos permite a hacer TS es darle un tipado a JS para que nuestra app sea escalable y ordenada.
- Concurrently: Se utiliza para poder ejecutar varios comandos a la vez. En este caso se utiliza primero para correr el build (npm run build), el cual transpila el código de TS a JS y lo guarda en el directorio dist, luego el start corre ese código ya transpilado a JS.
- Webpack: es un empaquetador de módulos, es decir, te permite generar un archivo único con todos aquellos módulos que necesita tu aplicación para funcionar. Esto ayuda a la performance de la aplicación.
- Eslint: son reglas para programar y darle un formato limpio al código.
- Jest: es una librería de testing unitario, justamente para correr pruebas en nuestro código.

